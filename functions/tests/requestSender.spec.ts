import HttpSender from "../src/services/httpSender";
import SslSender from "../src/services/sslSender";
import urlParser from "../src/services/urlParser";
import { expect } from "chai";
import isUrlValid from "../src/services/urlVadidator";

const urls: string[] = ["https://google.com", "https://seznam.cz"];
const sender = new HttpSender(urls);
const sslSender = new SslSender([...urls, "https://expired.badssl.com/"]);

describe("Request Sender", () => {
  describe("URL parsing", () => {
    it("should return parsed url addresses", () => {
      const parsed = urlParser(urls);
      expect(urls[1]).to.not.equal(parsed[1]);
    });
  });
  describe("URL validation", () => {
    it("should work if url exists", done => {
      isUrlValid("google.com").then(res => {
        expect(res).to.be.a("boolean");
        done();
      });
    });
    it("should work if url doesn't exists", done => {
      isUrlValid("fakegoogle").then(res => {
        expect(res).to.be.a("boolean");
        done();
      });
    });
    it("should work if url has expired ssl", done => {
      isUrlValid("https://expired.badssl.com/").then(res => {
        expect(res).to.be.a("boolean");
        done();
      });
    });
  });

  describe("HTTP request sending", () => {
    it("should send request to existing URL addresses and return array of information about their status", done => {
      sender
        .checkAllUrls()
        .then(res => {
          expect(res).to.be.an("array");
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });

  describe("SSL request sending", () => {
    it("should send request to existing URL addresses and return array of information about their SSL certificate", done => {
      sslSender
        .checkAllUrls()
        .then(res => {
          expect(res).to.be.an("array");
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
});
