import * as functions from "firebase-functions";
import check from "./api/controller";

export const get = functions.https.onRequest(check);
