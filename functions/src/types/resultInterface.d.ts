export interface Result {
  status: boolean;
  payload: payload;
}

export type payload = string | object;

export default Result;
