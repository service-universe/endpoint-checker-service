import { queryCallback } from "mysql";

export interface DatabaseInterface {
  connect(): Promise<boolean>;
  execute(query: string): Promise<SQLResponse>;
  disconnect(): Promise<boolean>;
}

export type SQLResponse = boolean | queryCallback;

export default DatabaseInterface;
