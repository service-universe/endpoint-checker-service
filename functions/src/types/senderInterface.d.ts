export default interface Sender {
  urls: string[];
  checkAllUrls(urls: string[]): Promise<object[]>;
}
