// Use this instead of json file.
const dotenv = require("dotenv");
dotenv.config();

// Replace file with ENV to keep bigger security.
const CONFIG = require("../configs/databaseConfig.json");

export default CONFIG;
