enum Error {
  missingUrl = "Sent body doesn't contain url parametr with string type.",
  badUrl = "Sent URL isn't valid, doesn't exist or has experied SSL certifiate.",
  httpError = "Http request ended with an error."
}

export default Error;
