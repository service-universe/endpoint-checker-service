import SslSender from "../services/sslSender";
import HttpSender from "../services/httpSender";
import isUrlValid from "../services/urlVadidator";
import { ResultSchema, ErrorResultSchema } from "../models/resultSchema";
import Error from "../configs/errorHandling";

const check = async (req: any, res: any) => {
  if (req.body.url && typeof req.body.url == "string") {
    const isValid = await isUrlValid(req.body.url);

    if (isValid) {
      const sslSender: SslSender = new SslSender([req.body.url]);

      const sslResponse: any = await sslSender.checkAllUrls().catch(() => {
        res.json(new ErrorResultSchema(Error.badUrl));
      });
      const httpSender: HttpSender = new HttpSender([req.body.url]);

      const httpResponse: any = await httpSender.checkAllUrls().catch(() => {
        res.json(new ErrorResultSchema(Error.httpError));
      });

      res.json(
        new ResultSchema({
          sslStatus: sslResponse[0],
          httpStatus: httpResponse[0]
        })
      );
    } else res.json(new ErrorResultSchema(Error.badUrl));
  } else res.json(new ErrorResultSchema(Error.missingUrl));
};

export default check;
