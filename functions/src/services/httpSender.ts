const sc = require("status-check");

import Sender from "../types/senderInterface";

class HttpSender implements Sender {
  public urls: string[];

  constructor(urls: string[]) {
    this.urls = urls;
  }

  /**
   * @name checkAllUrls
   * @description Method for URL checking.
   * @returns {Promise} - Request results representing status information of endpoint.
   */
  public checkAllUrls(): Promise<object[]> {
    return new Promise((resolve, reject) => {
      sc.startCheckingLink(this.urls, (res: any) => {
        resolve(res);
      });
    });
  }
}

export default HttpSender;
