import Sender from "../types/senderInterface";
import parser from "./urlParser";

const sslChecker = require("ssl-checker");

class SslSender implements Sender {
  public urls: string[];

  constructor(urls: string[]) {
    this.urls = parser(urls);
  }

  /**
   * @name checkAllUrls
   * @description Method for URL SSL status checking.
   * @returns {Promise} - Request results representing SSL certificate information of endpoint.
   */
  public checkAllUrls(): Promise<object[]> {
    return new Promise(async (resolve, reject) => {
      const promises = this.urls.map(async url => {
        return await sslChecker(url);
      });
      const results = await Promise.all(promises);
      resolve(results);
    });
  }
}

export default SslSender;
