const parser = require("parse-url");

/**
 * @name parse
 * @description Method for URL parsing
 * @param  {array} urls - URL addresses for parsing.
 * @returns {array} - Parsed URL addresses (https://google.com -> google.com)
 */
const parse = (urls: string[]): string[] => {
  return urls.map((url: string) => {
    return parser(url).resource;
  });
};

export default parse;
