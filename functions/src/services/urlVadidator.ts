const urlExists = require("url-exists");
/**
 * @name isUrlValid
 * @description Method for URL validation control.
 * @param  {string} url - Target URL for validation control.
 * @returns {Promise} - Validation result represented as boolean.
 */
export const isUrlValid = (url: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    urlExists(url, (err: any, exists: any) => {
      if (err) reject(true);
      resolve(!!exists);
    });
  });
};

export default isUrlValid;
