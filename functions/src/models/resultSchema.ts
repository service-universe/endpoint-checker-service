import { Result, payload } from "../types/resultInterface";

export class ResultSchema implements Result {
  public payload: payload;
  public status: boolean;
  constructor(payload: object) {
    this.status = true;
    this.payload = payload;
  }
}

export class ErrorResultSchema implements Result {
  public payload: payload;
  public status: boolean;
  constructor(payload: string) {
    this.payload = payload;
    this.status = false;
  }
}

export default ResultSchema;
