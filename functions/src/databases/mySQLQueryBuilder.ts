// Can be replaced with ORM, Library, or you can just simply enter query into function... It's just a prototype that i am going to recreate to library in the future.
// Not the greatest code but it works. I am thinking about rewrite it to prototypes or something.
class MySQLQueryBuilder {
  static select(items: string) {
    return {
      from: (table: string) => {
        return {
          query: `SELECT ${items} FROM ${table}`,
          where: (keys: string, values: string) => {
            return {
              query: `SELECT ${items} FROM ${table} WHERE ${keys} LIKE ${values}`
            };
          }
        };
      }
    };
  }

  static insert(items: any): any {
    return {};
  }

  static update(items: any): any {
    return {};
  }

  static delete(table: any): any {
    return {};
  }
}

export default MySQLQueryBuilder;
