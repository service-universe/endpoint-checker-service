import { createConnection, Connection, MysqlError, queryCallback } from "mysql";
import {
  DatabaseInterface as Database,
  SQLResponse
} from "../types/databaseInterface";

// Replace file with ENV to keep bigger security.
const CONFIG = require("../configs/databaseConfig").MySQL;

// Creating SQL connection.
const connection: Connection = createConnection(CONFIG);

class MySQLDatabase implements Database {
  /**
   * @name connect
   * @description Method for connecting to MySQL database.
   * @returns Promise
   */
  public connect(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      connection.connect((err: MysqlError) => {
        if (err) {
          console.error(err);
          reject(true);
        }
        console.log("Database connected!");
        resolve(true);
      });
    });
  }

  /**
   * @name execute
   * @description Method for executing SQL query in MySQL database.
   * @param  {string} query - SQL query string. (example: SELECT * FROM table)
   * @returns {Promise} - Database response.
   */
  public execute(query: string): Promise<SQLResponse> {
    return new Promise((resolve, reject) => {
      connection.query(query, (err: MysqlError, res: queryCallback) => {
        if (err) {
          console.error(err);
          reject(true);
        }
        resolve(res);
      });
    });
  }

  /**
   * @name disconnect
   * @description Method for disconnecting from MySQL database.
   * @returns {Promise} - Database response.
   */
  public disconnect(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      connection.end((err: MysqlError) => {
        if (err) {
          console.error(err);
          reject(true);
        }
        console.log("Database disconnected!");
        resolve(true);
      });
    });
  }
}

export default MySQLDatabase;
